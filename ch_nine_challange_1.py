
def enrollment_stats(list_of_universities):
    #  variables
    total_students = []
    total_tuition = []

    # iterate through the list
    for universty in list_of_universities:
        total_students.append(universty[1])
        total_tuition.append(universty[2])
    # return variables
    return total_students, total_tuition


def mean(values):
    """Return the mean value in the list values"""
    return sum(values) / len(values)

def median(values):
    """Return the median in the list values"""
    values.sort()
    # if the number of the value is odd,
    # return the middle value of the list
    if len(values) % 2 == 1:
        # value at the center of the list in the value
        # at whose index is half od the list
        # rounded down
        center_index = int(len(values) / 2)
        return values[center_index]
    # otherwise, if the length is even, return
    # the mean of the two center value
    else:
        left_center_index = (len(values) - 1) // 2
        right_center_index = (len(values) + 1) // 2
        return mean([values[left_center_index], values[right_center_index]])


universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

total = enrollment_stats(universities)


print("\n")
print("*****" * 6)
print(f"Total students: {sum(total[0]):,}")
print(f"Total tuition: $ {sum(total[1]):,}")
print(f"\nStudent mean: {mean(total[0]):,.2f}")
print(f"Student median: {median(total[0]):,}")
print(f"\nTuition mean: $ {mean(total[1]):,.2f}")
print(f"Tuition median: $ {median(total[1]):,}")
print("*****" * 6)