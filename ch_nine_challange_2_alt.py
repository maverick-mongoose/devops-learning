import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant","exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon","for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def make_poem():
    r_nouns = random.choices(nouns, k=3)
    r_verb = random.choices(verbs, k=3)
    r_adj = random.choices(adjectives, k=3)
    r_prep = random.choices(prepositions, k=2)
    r_adv = random.choice(adverbs)

    # check 1st letter is vowel for adj
    if "aeiou".find(r_adj[0]) != -1:
        article1 = "An"
    else:
        article1 = "A"

    if "aeiou".find(r_nouns[0]) != -1:
        article2 = "An"
    else:
        article2 = "A"

    poem = (
        f"{article1} {r_adj[0]} {r_nouns[0]}\n\n"
        f"{article2} {r_nouns[0]} {r_verb[0]} {r_prep[0]} the {r_adj[1]} {r_nouns[1]}\n"
        f"{r_adv}, the {r_nouns[0]} {r_verb[1]}\n"
        f"the {r_nouns[1]} {r_verb[2]} {r_prep[1]} a {r_adj[2]} {r_nouns[2]}"
    )

    return poem

poem = make_poem()
print(poem)