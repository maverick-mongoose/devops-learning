import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

# state = random.choice(list(capitals_dict.keys()))
# capital = capitals_dict[state]

state , capital = random.choice(list(capitals_dict.items()))


print(f"Name of the state is '{state}.'")

while True:
    user_ans = input(f"What is the capital of {state} :").lower()

    if user_ans == 'exit':
        print(f"correct answer is : {capital}, Goodbye !")
        break
    elif user_ans == capital.lower():
        print(f"Correct!")
        break
    else:
        print(f"incorrect ! Try again")